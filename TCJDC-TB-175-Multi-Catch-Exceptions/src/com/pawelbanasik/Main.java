package com.pawelbanasik;

import java.util.InputMismatchException;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		try {
			int result = divide();
			System.out.println(result);
			
		} catch (ArithmeticException | NoSuchElementException e) {
			System.out.println(e.toString());
			System.out.println("Unable to perform division, autopilot shutting down");
		}

	}
	// zmienil metode - multi catch - lapiemy kilka wyjatkow
	public static int divide() {
		int x, y;
//		try {
			x = getInt();
			y = getInt();
			System.out.println("x is " + x + ", y is " + y);
			return x / y;
//		} catch (NoSuchElementException e) {
//			throw new NoSuchElementException("no suitable input");
//		} catch (ArithmeticException e) {
//			throw new ArithmeticException("attempt to divide by zero");
//		}

	}

	// dziala tak ze dopoki nie wpiszemy poprawnie danych
	private static int getInt() {
		Scanner s = new Scanner(System.in);
		System.out.println("Please enter an integer ");
		while (true) {
			try {
				return s.nextInt();
			} catch (InputMismatchException e) {
				s.nextLine();
				System.out.println("Please enter a number using only the digits 0 to ");
			}
		}
	}

}
